
<%--
 Created by IntelliJ IDEA.
 User: student66
 Date: 10/22/20
 Time: 2:11 PM
 To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add a new blog post</title>
</head>
<body>
    <%@include file="/partials/navbar.jsp"%>

    <center>
        <div class="container">
            <form action="/add-post" method="post">
                <label for="title">Title</label>
                <input type="text" id="title" name="title">
                <br>
                <label for="author">Author</label>
                <input type="text" id="author" name="author">
                <br>
                Appddiction Studio <input type="checkbox" name="categories" value="Appddiction Studio"> |
                Codebound <input type="checkbox" name="categories" value="Codebound"> |
                Developers <input type="checkbox" name="categories" value="Developers"> |
                Instructors <input type="checkbox" name="categories" value="Instructors"> |
                Course <input type="checkbox" name="categories" value="Course">
                <br>
                <label for="content">Content</label>
                <textarea id="content" name="content" cols="100" rows="6"></textarea>
                <br>
                <button>Add Post</button>
            </form>
            <form action="/display-all-posts">
                <button>Return back to posts</button>
            </form>
        </div>
    </center>
    <%@include file="/partials/footer.jsp"%>
</body>
</html>

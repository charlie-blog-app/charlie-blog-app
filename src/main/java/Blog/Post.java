package Blog;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Post {
    //    this is everything I could think of that a post would have.
    private int id;
    private String postTitle, postContent, author;
    private String[] categories;
    private int[] date;

//    constructor. left the categories as an array list but since the date is always day/month/year had the variables separate and they are put together by the constructor
// can use if/else or switch on the date array to display January/February/etc and Monday/Tuesday/etc

    public Post(String postTitle, String postContent, String author, String[] categories, int day, int month, int year, int hour, int minute, int second) {
//        this.id = id;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.author = author;
        this.categories = categories;
        this.date = new int[]{day, month, year, hour, minute, second};
    }


    //    getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public int[] getDate() {
        return date;
    }

    //    had the setter for the date match the way it is in the constructor
    public void setDate(int day, int month, int year, int hour, int minute, int second) {
        this.date[0] = day;
        this.date[1] = month;
        this.date[2] = year;
        this.date[3] = hour;
        this.date[4] = minute;
        this.date[5] = second;
    }

    public String toString(){
//        converting most of the post to a String
        String output = this.id + "▄" + this.postTitle + "▄" + this.postContent + "▄" + this.author + "▄";
//        taking the category ArrayList and splitting it up into Strings
        int loop = 0;
        for (String cat : this.categories) {
            if (loop < this.categories.length-1) {
                output += cat + "▀";
                loop++;
            } else {
                output += cat;
            }
        }
//        putting the rest of the post to a String
        output += "▄";
        output += this.date[0];
        output += "▄";
        output += this.date[1];
        output += "▄";
        output += this.date[2];
        output += "▄";
        output += this.date[3];
        output += "▄";
        output += this.date[4];
        output += "▄";
        output += this.date[5];

        return output;
    }

    public Post(int id, String postTitle, String postContent, String author, String[] categories, int day, int month, int year, int hour, int minute, int second) {
        this.id = id;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.author = author;
        this.categories = categories;
        this.date = new int[]{day, month, year, hour, minute, second};
    }
}

<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 2:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../css/style.css">
<%--    <%@include file="/css/style.css"%>--%>
</head>
<body>

    <nav class="nav">
        <div class="nav-menu flex-row">
            <div class="nav-brand">
                <a href="#" class="text-gray">CodeBound Texas Blog</a>
            </div>
            <div class="toggle-collapse">
                <div class="toggle-icons">
                    <img src="https://img.icons8.com/fluent/24/000000/menu--v1.png" alt=""/>
                </div>
            </div>
            <div>
                <ul class="nav-items">
                    <li class="nav-link">
                        <a href="../index.jsp">Home</a>
                    </li>
                    <li class="nav-link">
                        <a href="../Blog-app/login.jsp">Blogs</a>
                    </li>
                    <li class="nav-link">
                        <a href="../Blog-app/login.jsp">Login</a>
                    </li>
                </ul>
            </div>
            <div class="social text-gray">
                <a href="https://www.facebook.com/" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/facebook-new.png" alt=""/></span></a>
                <a href="https://www.instagram.com/?hl=en" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" alt=""/></span></a>
                <a href="https://twitter.com/login?lang=en-gb" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/twitter.png" alt=""/></span></a>
                <a href="https://www.linkedin.com/" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/linkedin.png" alt=""/></span></a>
            </div>
        </div>
    </nav>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="../js/main.js"></script>
<%--    <%@include file="/js/main.js"%>--%>
    <hr style="color: red">
</body>
</html>

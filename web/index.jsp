<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Home</title>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/aos/aos.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="shortcut icon" href="images/favicon.ico" />
</head>
<body>
  <%@include file="partials/navbar.jsp"%>
  <main>
    <section class="site-title">
      <div class="site-background" data-aos="fade-up" data-aos-delay="100">
        <h3>Appddiction Codebound</h3>
        <h1>Charlie Cohort Blog Site</h1>
        <button class="btn">Explore</button>
      </div>
    </section>
    <section>
      <div class="carousel">
        <div class="container">
          <div class="owl-carousel owl-theme spinner-post">
            <div class="spinner-content" data-aos="fade-right" data-aos-delay="200">
              <img src="images/post-5.jpeg" alt="post-1" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-in" data-aos-delay="200">
              <img src="images/post-2.jpeg" alt="post-2" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-left" data-aos-delay="200">
              <img src="images/post-3.jpg" alt="post-3" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-right" data-aos-delay="200">
              <img src="images/post-4.jpeg" alt="post-4" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-in" data-aos-delay="200">
              <img src="images/post-6.jpeg" alt="post-5" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-left" data-aos-delay="200">
              <img src="images/post-7.jpg" alt="post-6" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-right" data-aos-delay="200">
              <img src="images/post-8.jpeg" alt="post-7" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-in" data-aos-delay="200">
              <img src="images/post-9.jpeg" alt="post-8" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
            <div class="spinner-content" data-aos="fade-left" data-aos-delay="200">
              <img src="images/post-10.jpeg" alt="post-9" height="270">
              <div class="spinner-title">
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <button class="btn btn-spinner">Info</button>
                <span>2 minutes</span>
              </div>
            </div>
          </div>
          <div class="owl-navigation">
            <span class="owl-nav-prev"><i class="fas fa-long-arrow-alt-left"></i></span>
            <span class="owl-nav-next"><i class="fas fa-long-arrow-alt-right"></i></span>
          </div>
        </div>
      </div>
    </section>
    <section class="container">
      <div class="site-content">
        <div class="posts">
          <div class="post-content" data-aos="zoom-in" data-aos-delay="200">
            <div class="post-image">
              <div>
                <img src="images/blog-1.jpeg" class="img" alt="blog-1">
              </div>
              <div class="post-info flex-row">
                <span class="text-gray"><i class="fas fa-user text-gray"></i>&nbsp;&nbsp;User</span>
                <span class="text-gray"><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;September 25, 2020</span>
                <span >2 comments</span>
              </div>
            </div>
            <div class="post-title">
              <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing
                elit. Atque blanditiis corporis ducimus, eveniet fuga obcaecati
                placeat reiciendis sequi vel?</a>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Alias amet atque deleniti doloribus excepturi hic, magnam
                vel? Commodi dolores earum est facere fuga illo iure labore
                obcaecati officia quis. Assumenda error harum laborum mollitia
                nisi quibusdam quo recusandae ullam voluptatum?</p>
              <button class="btn post-btn">Read More &nbsp; <i class="fas fa-arrow-right"></i></button>
            </div>
          </div>
          <hr>
          <div class="post-content" data-aos="zoom-in" data-aos-delay="200">
            <div class="post-image">
              <div>
                <img src="images/blog-2.jpeg" class="img" alt="blog-1">
              </div>
              <div class="post-info flex-row">
                <span class="text-gray"><i class="fas fa-user text-gray"></i>&nbsp;&nbsp;User</span>
                <span class="text-gray"><i></i><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;September 25, 2020</span>
                <span >2 comments</span>
              </div>
            </div>
            <div class="post-title">
              <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing
                elit. Atque blanditiis corporis ducimus, eveniet fuga obcaecati
                placeat reiciendis sequi vel?</a>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Alias amet atque deleniti doloribus excepturi hic, magnam
                vel? Commodi dolores earum est facere fuga illo iure labore
                obcaecati officia quis. Assumenda error harum laborum mollitia
                nisi quibusdam quo recusandae ullam voluptatum?</p>
              <button class="btn post-btn">Read More &nbsp; <i class="fas fa-arrow-right"></i></button>
            </div>
          </div>
          <hr>
          <div class="post-content" data-aos="zoom-in" data-aos-delay="200">
            <div class="post-image">
              <div>
                <img src="images/blog-3.jpeg" class="img" alt="blog-1">
              </div>
              <div class="post-info flex-row">
                <span class="text-gray"><i class="fas fa-user text-gray"></i>&nbsp;&nbsp;User</span>
                <span class="text-gray"><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;September 25, 2020</span>
                <span >2 comments</span>
              </div>
            </div>
            <div class="post-title">
              <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing
                elit. Atque blanditiis corporis ducimus, eveniet fuga obcaecati
                placeat reiciendis sequi vel?</a>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Alias amet atque deleniti doloribus excepturi hic, magnam
                vel? Commodi dolores earum est facere fuga illo iure labore
                obcaecati officia quis. Assumenda error harum laborum mollitia
                nisi quibusdam quo recusandae ullam voluptatum?</p>
              <button class="btn post-btn">Read More &nbsp; <i class="fas fa-arrow-right"></i></button>
            </div>
          </div>
          <hr>
          <div class="post-content" data-aos="zoom-in" data-aos-delay="200">
            <div class="post-image">
              <div>
                <img src="images/blog-4.gif" class="img" alt="blog-1">
              </div>
              <div class="post-info flex-row">
                <span class="text-gray"><i class="fas fa-user text-gray"></i>&nbsp;&nbsp;User</span>
                <span class="text-gray"><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;September 25, 2020</span>
                <span >2 comments</span>
              </div>
            </div>
            <div class="post-title">
              <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing
                elit. Atque blanditiis corporis ducimus, eveniet fuga obcaecati
                placeat reiciendis sequi vel?</a>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Alias amet atque deleniti doloribus excepturi hic, magnam
                vel? Commodi dolores earum est facere fuga illo iure labore
                obcaecati officia quis. Assumenda error harum laborum mollitia
                nisi quibusdam quo recusandae ullam voluptatum?</p>
              <button class="btn post-btn">Read More&nbsp;</button>
            </div>
          </div>
          <div class="pagination flex-row">
            <a href="#"><i class="fas fa-chevron-left"></i></a>
            <a href="#" class="pages">1</a>
            <a href="#" class="pages">2</a>
            <a href="#" class="pages">3</a>
            <a href="#"><i class="fas fa-chevron-right"></i></a>
          </div>
        </div>
        <aside class="sidebar">
          <div class="category">
            <h2>Category</h2>
            <ul class="category-list">
              <li class="list-items" data-aos="fade-left" data-aos-delay="100">
                <a href="#">Appddiction Studio</a>
                <span>(05)</span>
              </li>
              <li class="list-items" data-aos="fade-left" data-aos-delay="200">
                <a href="#">Codebound</a>
                <span>(04)</span>
              </li>
              <li class="list-items" data-aos="fade-left" data-aos-delay="300">
                <a href="#">Developers</a>
                <span>(07)</span>
              </li>
              <li class="list-items" data-aos="fade-left" data-aos-delay="400">
                <a href="#">Instructors</a>
                <span>(08)</span>
              </li>
              <li class="list-items" data-aos="fade-left" data-aos-delay="500">
                <a href="#">Course</a>
                <span>(08)</span>
              </li>
            </ul>
          </div>
          <div class="popular-post">
            <h2>Popular Post</h2>
            <div class="post-content" data-aos="flip-up" data-aos-delay="200">
              <div class="post-image">
                <div>
                  <img src="images/blog-1.jpeg" class="img" alt="blog1">
                </div>
                <div class="post-info flex-row">
                  <span><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;January 14, 2019</span>
                  <span>2 Comments</span>
                </div>
              </div>
              <div class="post-title">
                <a href="#">New data recording system to better analyse road accidents</a>
              </div>
            </div>
            <div class="post-content" data-aos="flip-up" data-aos-delay="300">
              <div class="post-image">
                <div>
                  <img src="images/blog-2.jpeg" class="img" alt="blog1">
                </div>
                <div class="post-info flex-row">
                  <span><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;January 14, 2019</span>
                  <span>2 Comments</span>
                </div>
              </div>
              <div class="post-title">
                <a href="#">New data recording system to better analyse road accidents</a>
              </div>
            </div>
            <div class="post-content" data-aos="flip-up" data-aos-delay="400">
              <div class="post-image">
                <div>
                  <img src="images/blog-3.jpeg" class="img" alt="blog1">
                </div>
                <div class="post-info flex-row">
                  <span><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;January 14, 2019</span>
                  <span>2 Comments</span>
                </div>
              </div>
              <div class="post-title">
                <a href="#">New data recording system to better analyse road accidents</a>
              </div>
            </div>
            <div class="post-content" data-aos="flip-up" data-aos-delay="500">
              <div class="post-image">
                <div>
                  <img src="images/blog-4.gif" class="img" alt="blog1">
                </div>
                <div class="post-info flex-row">
                  <span><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;January 14, 2019</span>
                  <span>2 Comments</span>
                </div>
              </div>
              <div class="post-title">
                <a href="#">New data recording system to better analyse road accidents</a>
              </div>
            </div>
            <div class="post-content" data-aos="flip-up" data-aos-delay="600">
              <div class="post-image">
                <div>
                  <img src="images/blog-5.jpeg" class="img" alt="blog1">
                </div>
                <div class="post-info flex-row">
                  <span><i class="fas fa-calendar-alt text-gray"></i>&nbsp;&nbsp;January 14, 2019</span>
                  <span>2 Comments</span>
                </div>
              </div>
              <div class="post-title">
                <a href="#">New data recording system to better analyse road accidents</a>
              </div>
            </div>
          </div>
          <div class="newsletter" data-aos="fade-up" data-aos-delay="300">
            <h2>Newsletter</h2>
            <div class="form-element">
              <input type="text" class="input-element" placeholder="Email">
              <button class="btn form-btn">Subscribe</button>
            </div>
          </div>
          <div class="popular-tags">
            <h2>Popular Tags</h2>
            <div class="tags flex-row">
              <span class="tag" data-aos="flip-up" data-aos-delay="100">Software</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="200">technology</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="300">travel</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="400">illustration</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="500">design</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="600">lifestyle</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="700">love</span>
              <span class="tag" data-aos="flip-up" data-aos-delay="800">project</span>
            </div>
          </div>
        </aside>
      </div>
    </section>
  </main>
  <%@include file="partials/footer.jsp"%>
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/aos/aos.js"></script>
  <script src="js/index.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" integrity="sha384-9/D4ECZvKMVEJ9Bhr3ZnUAF+Ahlagp1cyPC7h5yDlZdXs4DQ/vRftzfd+2uFUuqS" crossorigin="anonymous"></script>
</body>
</html>

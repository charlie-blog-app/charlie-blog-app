package Blog;

import java.util.List;

public interface Posts {
    List<Post> all();

    void addPost(Post newPost);
    void editPost(Post edit);
    void deletePost(Post delete);
}

package Blog;

public class PostsDaoFactory {
    private static Posts postsDao;
    public static  Posts getPostsDao(){
        if (postsDao == null){
            postsDao = new ListPosts();
        }

        return postsDao;
    }
}

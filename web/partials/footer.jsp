<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/23/20
  Time: 11:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../css/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/aos/aos.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">

</head>
<body>

<footer class="footer">
    <div class="container">
        <div class="about-us" data-aos="fade-right" data-aos-delay="200">
            <h2>About us</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium quia atque nemo ad modi officiis
                iure, autem nulla tenetur repellendus.</p>
        </div>
        <div class="newsletter" data-aos="fade-right" data-aos-delay="200">
            <h2>Newsletter</h2>
            <p>Stay update with our latest</p>
            <div class="form-element">
                <input type="text" placeholder="Email"><span><i class="fas fa-chevron-right"></i></span>
            </div>
        </div>
        <div class="instagram" data-aos="fade-left" data-aos-delay="200">
            <h2>Instagram</h2>
            <div class="flex-row">
                <img src="../images/thumbcard-3.jpg" alt="insta1">
                <img src="../images/thumbcard-4.jpeg" alt="insta2">
                <img src="../images/thumbcard-5.jpeg" alt="insta3">
            </div>
            <div class="flex-row">
                <img src="../images/thumbcard-6.jpeg" alt="insta4">
                <img src="../images/thumbcard-7.jpg" alt="insta5">
                <img src="../images/thumbcard-8.png" alt="insta6">
            </div>
        </div>
        <div class="follow" data-aos="fade-left" data-aos-delay="200">
            <h2>Follow us</h2>
            <p>Let us be Social</p>
            <div>
                <a href="https://www.facebook.com/" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/facebook-new.png" alt=""/></span></a>
                <a href="https://www.instagram.com/?hl=en" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" alt=""/></span></a>
                <a href="https://twitter.com/login?lang=en-gb" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/twitter.png" alt=""/></span></a>
                <a href="https://www.linkedin.com/" target="_blank"><span><img src="https://img.icons8.com/fluent/48/000000/linkedin.png" alt=""/></span></a>
            </div>
        </div>
    </div>
    <div class="rights flex-row">
        <h4 class="text-gray">
            Copyright ©2019 All rights reserved | made by | Charlie Cohort
        </h4>
    </div>
    <div class="move-up">
        <span><i class="fas fa-arrow-circle-up fa-2x"></i></span>
    </div>
</footer>

<script src="../js/aos/aos.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" integrity="sha384-9/D4ECZvKMVEJ9Bhr3ZnUAF+Ahlagp1cyPC7h5yDlZdXs4DQ/vRftzfd+2uFUuqS" crossorigin="anonymous"></script>
</body>
</html>

package Blog;

import java.util.ArrayList;

import java.util.List;

public class ListPosts implements Posts{
    private ArrayList<Post> postsList;

    public ListPosts(){
        this.postsList = new ArrayList<>();
    }

    public List<Post> all() {
        return this.postsList;
    }


    @Override
    public void addPost(Post newPost){
        if (postsList.isEmpty()){
            newPost.setId(1);
        } else {
            int set = 0;
            for (Post currentPosts : postsList){
                if (currentPosts.getId() > set){
                    set = currentPosts.getId();
                }
            }
            set += 1;
            newPost.setId(set);
        }
        postsList.add(newPost);
    }

    public void editPost(Post edit) {
        for (Post currentPosts : postsList) {
            if (currentPosts.getId() == edit.getId()) {
                for (Post x : postsList){
                    if (x.getId() == edit.getId()){
                        postsList.remove(x);
                        postsList.add(edit);
                    }
                }
                return;
            }
        }
        System.out.println("Post ID " + edit.getId() + " not found");
    }

    public void deletePost(Post delete) {
        for (Post currentPosts : postsList) {
            if (currentPosts.getId() == delete.getId()) {
                postsList.removeIf(x -> x.getId() == delete.getId());
                return;
            }
        }
        System.out.println("Post ID " + delete.getId() + " not found");
    }
}

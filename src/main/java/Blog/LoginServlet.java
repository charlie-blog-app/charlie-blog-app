package Blog;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

    @WebServlet(name = "LoginServlet", urlPatterns = "/login")
    public class LoginServlet extends HttpServlet {
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            String username = request.getParameter("username");
            String password = request.getParameter("password");

            boolean validAttempt = username.equals("admin") && password.equals("password");

            if (validAttempt) {
                request.getSession().setAttribute("user", true);

                request.getSession().setAttribute("username", username);

                response.sendRedirect("/display-all-posts");
            }
            else {

                response.sendRedirect("/LoginError");
            }

        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            HttpSession session = request.getSession();

            boolean isUser = false;

            if (session.getAttribute("user") != null) {

                isUser = (boolean) session.getAttribute("user");



            }

            if (isUser) {

                request.getRequestDispatcher("/display-all-posts.jsp").forward(request, response);
            }
            else {

                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
        }
    }


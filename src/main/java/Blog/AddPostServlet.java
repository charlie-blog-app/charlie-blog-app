package Blog;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@WebServlet(name = "AddPostsServlet", urlPatterns = "/add-post")
public class AddPostServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Posts postDao = PostsDaoFactory.getPostsDao();
        String newPostTitle = request.getParameter("title");
        String newPostContent = request.getParameter("content");
        String newPostAuthor = request.getParameter("author");
        String[] Categories = request.getParameterValues("categories");
        DateTimeFormatter d = DateTimeFormatter.ofPattern("dd");
        DateTimeFormatter m = DateTimeFormatter.ofPattern("MM");
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyy");
        DateTimeFormatter hr = DateTimeFormatter.ofPattern("hh");
        DateTimeFormatter min = DateTimeFormatter.ofPattern("mm");
        DateTimeFormatter s = DateTimeFormatter.ofPattern("ss");
        LocalDateTime t = LocalDateTime.now();
        int day = Integer.parseInt(d.format(t));
        int month = Integer.parseInt(m.format(t));
        int year = Integer.parseInt(y.format(t));
        int hour = Integer.parseInt(hr.format(t));
        int minute = Integer.parseInt(min.format(t));
        int second = Integer.parseInt(s.format(t));
        Post newPost = new Post(newPostTitle, newPostContent, newPostAuthor, Categories, day, month, year, hour, minute, second);
        postDao.addPost(newPost);
        response.sendRedirect("/display-all-posts");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/Blog-app/add-post.jsp");
    }
}


package Blog;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DisplayPostsServlet", urlPatterns = "/display-all-posts")

public class DisplayPostsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Posts postDao = PostsDaoFactory.getPostsDao();
        List<Post> allPosts = postDao.all();
        request.setAttribute("allPosts", allPosts);
        request.getRequestDispatcher("/Blog-app/display-all-posts.jsp").forward(request, response);

    }
}

<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 2:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logout</title>
    <link rel="shortcut icon" href="../images/favicon.ico" />

    <style>
        body {
            background-image: url("../images/LoginLogoutBackground.png");
            background-size: 100%;
            background-repeat: no-repeat;
        }

    </style>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>

    <center>
        <div class="container">
            <h1>You have successfully logged out</h1>
            <a href="/Blog-app/login.jsp"><h2>Would you like to use another account?</h2></a>
        </div>
        <div class="container">
            <a href="/index.jsp">Home</a>
            <br>
            <a href="/Blog-app/login.jsp">Login</a>
        </div>
    </center>

</body>
</html>

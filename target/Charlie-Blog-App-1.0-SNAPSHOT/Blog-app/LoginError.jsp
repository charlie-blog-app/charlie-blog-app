<%--
  Created by IntelliJ IDEA.
  User: student02
  Date: 11/2/20
  Time: 2:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Error</title>
    <link rel="shortcut icon" href="../images/favicon.ico" />
    <style>
        body {
            background-image: url("../images/LoginLogoutBackground.png");
            background-size: 100%;
            background-repeat: no-repeat;
        }
    </style>
</head>
<body>
<%@include file="../partials/navbar.jsp"%>
<div STYLE="text-align: center">
<h1>Error!</h1>
<h2>Invalid Username or Password!</h2>
<h3> Click <a href="/Blog-app/login.jsp" style="color: red; text-decoration-line: underline">HERE</a> to be redirected.</h3>
</div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 2:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="../css/login.css">
    <link rel="shortcut icon" href="../images/favicon.ico" />

    <style>
        body {
            background-image: url("../images/BackgroundRedVector.png");
            background-size: cover;
            background-repeat: no-repeat;
        }
        .login {
            background-image: url("../images/CodeBoundLogoi.png");
            background-size: cover;
            background-repeat: no-repeat;

            border: 1px solid darkred;
            border-radius: 6px;
            height: 400px;
            margin: 20px auto 0;
            width: 600px;

        /*    code didnt work in css folder so i put it here. Trying to make the login menu larger and with red buttons with a white background,
        same to the theme of codebound.*/
        }
        #username {
            margin-top: 100px
        }

    </style>
</head>
<body>
<%@include file="../partials/navbar.jsp"%>

<center>
    <div class="container">
        <form action="/login" method="post">
            <div class="login">
                <div class="form-group">
                    <input type="text" placeholder="Username" id="username" name="username">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" id="password" name="password">
                </div>
                <a href="#" class="forgot">forgot password?</a>
                <input type="submit" value="Log In">
            </div>
            <div class="shadow"></div>
        </form>
    </div>
</center>

</body>
</html>

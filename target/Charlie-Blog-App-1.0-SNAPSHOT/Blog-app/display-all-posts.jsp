
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
 Created by IntelliJ IDEA.
 User: student66
 Date: 10/22/20
 Time: 2:10 PM
 To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Blog</title>
    <link rel="shortcut icon" href="../images/favicon.ico" />

</head>
<body>
    <%@include file="/partials/navbar.jsp"%>

    <center>
        <div class="container">
            <h1>Blog Posts</h1>

            <c:forEach items="${allPosts}" var="post">
                <br>
                <table border="1" cellpadding="1">
                    <tr><h3>
                        <th> ${post.postTitle} </th>
                        <th> By: ${post.author}</th>
                        <th> Post ${post.id}</th>
                    </h3></tr>
                    <tr>
                        <td colspan="3"> ${post.postContent} </td>
                    </tr>
                    <tr>
                        <h4><td colspan="3">  Written <c:choose>
                            <c:when test="${post.date[1] == 1}">
                                January
                            </c:when>
                            <c:when test="${post.date[1] == 2}">
                                February
                            </c:when>
                            <c:when test="${post.date[1] == 3}">
                                March
                            </c:when>
                            <c:when test="${post.date[1] == 4}">
                                April
                            </c:when>
                            <c:when test="${post.date[1] == 5}">
                                May
                            </c:when>
                            <c:when test="${post.date[1] == 6}">
                                June
                            </c:when>
                            <c:when test="${post.date[1] == 7}">
                                July
                            </c:when>
                            <c:when test="${post.date[1] == 8}">
                                August
                            </c:when>
                            <c:when test="${post.date[1] == 9}">
                                September
                            </c:when>
                            <c:when test="${post.date[1] == 10}">
                                October
                            </c:when>
                            <c:when test="${post.date[1] == 11}">
                                November
                            </c:when>
                            <c:when test="${post.date[1] == 12}">
                                December
                            </c:when>
                        </c:choose> ${post.date[0]} ${post.date[2]} at ${post.date[3]}:${post.date[4]}:<c:choose><c:when test="${post.date[5]<=9}">0${post.date[5]}</c:when><c:when test="${post.date[5] >= 10}">${post.date[5]}</c:when></c:choose>
                        </td></h4>
                    </tr>
                </table>
                <table border="1" cellpadding="2">
                    <tr>
                        <c:forEach items="${post.categories}" var="categories">
                            <th> ${categories} </th>
                        </c:forEach>
                    </tr>
                </table>
            </c:forEach>
            <br>
            <form action="/Blog-app/add-post.jsp">
                <button>Click to add a post</button>
            </form>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <form action="/logout">
                <button>Logout</button>
            </form>
        </div>
    </center>
    <%@include file="/partials/footer.jsp"%>
</body>
</html>
